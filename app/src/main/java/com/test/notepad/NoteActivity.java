package com.test.notepad;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.EditText;

import java.util.Date;

public class NoteActivity extends AppCompatActivity {

    EditText edTitle, edText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        edTitle = (EditText) findViewById(R.id.tv_title);
        edText = (EditText) findViewById(R.id.tv_text);

        if (getIntent().getExtras().getInt("code") == MainActivity.READ_NOTE) {
            Note note = Note.getNote(getIntent().getExtras().getLong("id"));
            edTitle.setText(note.getTitle());
            edText.setText(note.getText());
            edTitle.setEnabled(false);
            edText.setEnabled(false);
        }

    }

    @Override
    public void onBackPressed() {
        if (getIntent().getExtras().getInt("code") == MainActivity.ADD_NOTE && edTitle.getText().toString().trim().length() > 0) {
            Note note = new Note(new Date().getTime(), edTitle.getText().toString(), edText.getText().toString());
            note.save();
        }
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
