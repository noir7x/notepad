package com.test.notepad;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Notes")
public class Note extends Model {
    @Column(name = "date")
    private long date;

    @Column(name = "title")
    private String title;

    @Column(name = "text")
    private String text;

    public Note() {
        super();
    }

    public Note(long date, String title, String text) {
        this.date = date;
        this.title = title;
        this.text = text;
    }

    public static Note getNote(long id) {
        return new Select().from(Note.class).where("Id = ?", id).executeSingle();
    }

    public static List<Note> getNotes() {
        return new Select().from(Note.class).execute();
    }

    public long getDate() {
        return date;
    }


    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

}
