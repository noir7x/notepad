package com.test.notepad;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotepadAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Note> noteList;
    private Context context;
    private int count;

    public NotepadAdapter(List<Note> noteList, Context context) {
        this.noteList = noteList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        count = 0;
    }

    public void setData(List<Note> noteList) {
        this.noteList = noteList;
    }

    @Override
    public int getCount() {
        return noteList.size();
    }

    @Override
    public Object getItem(int position) {
        return noteList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return noteList.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.item_note, parent, false);

        ((TextView) view.findViewById(R.id.tv_date)).setText(getDate(noteList.get(position).getDate()));
        ((TextView) view.findViewById(R.id.tv_title)).setText(noteList.get(position).getTitle());


        switch (count) {
            case 0:
                view.setBackgroundColor(Color.RED);
                break;
            case 1:
                view.setBackgroundColor(Color.BLUE);
                break;
            case 2:
                view.setBackgroundColor(Color.GREEN);
                break;
        }

        count++;

        if (count == 3)
            count = 0;

        return view;
    }

    private String getDate(long timestamp) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        return dateFormat.format(new Date(timestamp));
    }
}
